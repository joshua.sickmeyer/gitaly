package featureflag

// PraefectGeneratedReplicaPaths will enable Praefect generated replica paths for new repositories.
var PraefectGeneratedReplicaPaths = NewFeatureFlag("praefect_generated_replica_paths", false)
