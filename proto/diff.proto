syntax = "proto3";

package gitaly;

import "lint.proto";
import "shared.proto";

option go_package = "gitlab.com/gitlab-org/gitaly/v14/proto/go/gitalypb";

// DiffService is a service which provides RPCs to inspect differences
// introduced between a set of commits.
service DiffService {

  // Returns stream of CommitDiffResponse with patches chunked over messages
  rpc CommitDiff(CommitDiffRequest) returns (stream CommitDiffResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // Return a stream so we can divide the response in chunks of deltas
  rpc CommitDelta(CommitDeltaRequest) returns (stream CommitDeltaResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // This comment is left unintentionally blank.
  rpc RawDiff(RawDiffRequest) returns (stream RawDiffResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // This comment is left unintentionally blank.
  rpc RawPatch(RawPatchRequest) returns (stream RawPatchResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // This comment is left unintentionally blank.
  rpc DiffStats(DiffStatsRequest) returns (stream DiffStatsResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

  // Return a list of files changed along with the status of each file
  rpc FindChangedPaths(FindChangedPathsRequest) returns (stream FindChangedPathsResponse) {
    option (op_type) = {
      op: ACCESSOR
    };
  }

}

// This comment is left unintentionally blank.
message CommitDiffRequest {
  // This comment is left unintentionally blank.
  enum DiffMode {
    // DEFAULT is the standard diff mode and results in a linewise diff for textfiles.
    DEFAULT = 0; // protolint:disable:this ENUM_FIELD_NAMES_PREFIX ENUM_FIELD_NAMES_ZERO_VALUE_END_WITH
    // WORDDIFF is a word diff and computes the diff for whitespace separated words instead of for whole lines.
    WORDDIFF = 1; // protolint:disable:this ENUM_FIELD_NAMES_PREFIX
  }

  // This comment is left unintentionally blank.
  Repository repository = 1 [(target_repository)=true];
  // This comment is left unintentionally blank.
  string left_commit_id = 2;
  // This comment is left unintentionally blank.
  string right_commit_id = 3;
  // This comment is left unintentionally blank.
  bool ignore_whitespace_change = 4;
  // This comment is left unintentionally blank.
  repeated bytes paths = 5;
  // This comment is left unintentionally blank.
  bool collapse_diffs = 6;
  // This comment is left unintentionally blank.
  bool enforce_limits = 7;

  // These limits are only enforced when enforce_limits == true.
  int32 max_files = 8;
  // This comment is left unintentionally blank.
  int32 max_lines = 9;
  // This comment is left unintentionally blank.
  int32 max_bytes = 10;
  // Limitation of a single diff patch,
  // patches surpassing this limit are pruned by default.
  // If this is 0 you will get back empty patches.
  int32 max_patch_bytes = 14;

  // These limits are only enforced if collapse_diffs == true.
  int32 safe_max_files = 11;
  // This comment is left unintentionally blank.
  int32 safe_max_lines = 12;
  // This comment is left unintentionally blank.
  int32 safe_max_bytes = 13;

  // DiffMode is the mode used for generating the diff. Please refer to the enum declaration for supported modes.
  DiffMode diff_mode = 15;
}

// A CommitDiffResponse corresponds to a single changed file in a commit.
message CommitDiffResponse {
  reserved 8;

  // This comment is left unintentionally blank.
  bytes from_path = 1;
  // This comment is left unintentionally blank.
  bytes to_path = 2;
  // Blob ID as returned via `git diff --full-index`
  string from_id = 3;
  // This comment is left unintentionally blank.
  string to_id = 4;
  // This comment is left unintentionally blank.
  int32 old_mode = 5;
  // This comment is left unintentionally blank.
  int32 new_mode = 6;
  // This comment is left unintentionally blank.
  bool binary = 7;
  // This comment is left unintentionally blank.
  bytes raw_patch_data = 9;
  // This comment is left unintentionally blank.
  bool end_of_patch = 10;
  // Indicates the diff file at which we overflow according to the limitations sent,
  // in which case only this attribute will be set.
  bool overflow_marker = 11;
  // Indicates the patch surpassed a "safe" limit and was therefore pruned, but
  // the client may still request the full patch on a separate request.
  bool collapsed = 12;
  // Indicates the patch was pruned since it surpassed a hard limit, and can
  // therefore not be expanded.
  bool too_large = 13;
}

// This comment is left unintentionally blank.
message CommitDeltaRequest {
  // This comment is left unintentionally blank.
  Repository repository = 1 [(target_repository)=true];
  // This comment is left unintentionally blank.
  string left_commit_id = 2;
  // This comment is left unintentionally blank.
  string right_commit_id = 3;
  // This comment is left unintentionally blank.
  repeated bytes paths = 4;
}

// This comment is left unintentionally blank.
message CommitDelta {
  // This comment is left unintentionally blank.
  bytes from_path = 1;
  // This comment is left unintentionally blank.
  bytes to_path = 2;
  // Blob ID as returned via `git diff --full-index`
  string from_id = 3;
  // This comment is left unintentionally blank.
  string to_id = 4;
  // This comment is left unintentionally blank.
  int32 old_mode = 5;
  // This comment is left unintentionally blank.
  int32 new_mode = 6;
}

// This comment is left unintentionally blank.
message CommitDeltaResponse {
  // This comment is left unintentionally blank.
  repeated CommitDelta deltas = 1;
}

// This comment is left unintentionally blank.
message RawDiffRequest {
  // This comment is left unintentionally blank.
  Repository repository = 1 [(target_repository)=true];
  // This comment is left unintentionally blank.
  string left_commit_id = 2;
  // This comment is left unintentionally blank.
  string right_commit_id = 3;
}

// This comment is left unintentionally blank.
message RawDiffResponse {
  // This comment is left unintentionally blank.
  bytes data = 1;
}

// This comment is left unintentionally blank.
message RawPatchRequest {
  // This comment is left unintentionally blank.
  Repository repository = 1 [(target_repository)=true];
  // This comment is left unintentionally blank.
  string left_commit_id = 2;
  // This comment is left unintentionally blank.
  string right_commit_id = 3;
}

// This comment is left unintentionally blank.
message RawPatchResponse {
  // This comment is left unintentionally blank.
  bytes data = 1;
}

// This comment is left unintentionally blank.
message DiffStatsRequest {
  // This comment is left unintentionally blank.
  Repository repository = 1 [(target_repository)=true];
  // This comment is left unintentionally blank.
  string left_commit_id = 2;
  // This comment is left unintentionally blank.
  string right_commit_id = 3;
}

// This comment is left unintentionally blank.
message DiffStats {
  // This comment is left unintentionally blank.
  bytes path = 1;
  // This comment is left unintentionally blank.
  int32 additions = 2;
  // This comment is left unintentionally blank.
  int32 deletions = 3;
  // This comment is left unintentionally blank.
  bytes old_path = 4;
}

// This comment is left unintentionally blank.
message DiffStatsResponse {
  // This comment is left unintentionally blank.
  repeated DiffStats stats = 1;
}

// Given a list of commits, return the files changed. Each commit is compared
// to its parent. Merge commits will show files which are different to all of
// its parents.
message FindChangedPathsRequest {
  // This comment is left unintentionally blank.
  Repository repository = 1 [(target_repository)=true];
  // This comment is left unintentionally blank.
  repeated string commits = 2;
}

// Returns a list of files that have been changed in the commits given
message FindChangedPathsResponse {
  // This comment is left unintentionally blank.
  repeated ChangedPaths paths = 1;
}

// Includes the path of the file, and the status of the change
message ChangedPaths {
  // This comment is left unintentionally blank.
  enum Status {
    // This comment is left unintentionally blank.
    ADDED = 0; // protolint:disable:this ENUM_FIELD_NAMES_PREFIX ENUM_FIELD_NAMES_ZERO_VALUE_END_WITH
    // This comment is left unintentionally blank.
    MODIFIED = 1; // protolint:disable:this ENUM_FIELD_NAMES_PREFIX
    // This comment is left unintentionally blank.
    DELETED = 2; // protolint:disable:this ENUM_FIELD_NAMES_PREFIX
    // This comment is left unintentionally blank.
    TYPE_CHANGE = 3; // protolint:disable:this ENUM_FIELD_NAMES_PREFIX
    // This comment is left unintentionally blank.
    COPIED = 4; // protolint:disable:this ENUM_FIELD_NAMES_PREFIX
  }

  // This comment is left unintentionally blank.
  bytes path = 1;
  // This comment is left unintentionally blank.
  Status status = 2;
}
